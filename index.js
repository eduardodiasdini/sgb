const app = require('./config/server');
const livrosRoutes = require('./app/routes/livrosRoutes');

console.log('[Index]');
livrosRoutes.getLivros(app);
livrosRoutes.getLivro(app);
livrosRoutes.addLivro(app);
livrosRoutes.updateLivro(app);
livrosRoutes.deleteLivro(app);
livrosRoutes.emprestimoLivro(app);
livrosRoutes.devolucaoLivro(app);
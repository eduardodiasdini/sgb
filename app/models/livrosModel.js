const client = require('../../config/dbConnection');

module.exports = class LivrosModel {
    static async getLivros() {
        console.log(`[getLivros]`);
        const cursor = await client.db("sgb").collection("livros").find();
        const livros = await cursor.toArray();
        return livros
    }

    static async getLivro(id) {
        console.log(`[getLivro] ID: ${id}`);

        try {
            const ObjectId = require('mongodb').ObjectId;
            const query = { _id: new ObjectId(id) };
            const livro = await client.db("sgb").collection("livros").findOne(query);

            if (!livro) {
                console.log('[getLivro] Livro não encontrado');
                return null;
            }

            return livro;
        } catch (error) {
            console.log('[getLivro] Ocorreu um erro:', error);
            throw error;
        }
    }

    static async addLivro(data) {
        console.log(`[addLivro]`);

        try {
            const newLivro = {
                titulo: data.titulo,
                autor: data.autor,
                ano: data.ano,
                adicionadoEm: new Date(),
                estaDisponivel: true,
                emprestadoPara: null,
                dataDoEmprestimo: null
            }

            const addedLivro = await client.db('sgb').collection('livros').insertOne(newLivro)
            return addedLivro
        } catch (error) {
            console.log('Ocorreu um erro: ', error)
        }
    }

    static async updateLivro(id, data) {
        console.log(`[updateLivro]`);

        try {
            const ObjectId = require('mongodb').ObjectId;
            const query = { _id: new ObjectId(id) };

            const livro = {
                $set: {
                    titulo: data.titulo,
                    autor: data.autor,
                    quantidadeTotal: data.quantidadeTotal,
                    quantidadeDisponivel: data.quantidadeDisponivel,
                    atualizadoEm: new Date()
                }
            };

            const options = { returnOriginal: false };
            const result = await client.db('sgb').collection('livros').findOneAndUpdate(query, livro, options);
            return result
        } catch (error) {
            console.log('Ocorreu um erro: ', error)
        }
    }

    static async deleteLivro(id) { 
        console.log(`[deleteLivro]`);

        try {
            const ObjectId = require('mongodb').ObjectId;
            const query = { _id: new ObjectId(id) };

            const result = await client.db('sgb').collection('livros').deleteOne(query);
            return result
        } catch(error) {
            console.log('Ocorreu um erro ao deletar o livro: ', error);
        }
    }

    static async emprestimoLivro(id) {
        console.log(`[emprestimoLivro]`);

        try {
            const ObjectId = require('mongodb').ObjectId;
            const query = { _id: new ObjectId(id) };
            console.log(`[emprestimoLivro] ID: ${JSON.stringify(query)}`);

            const livro = await client.db('sgb').collection('livros').findOne(query);

            if (!livro) {
                throw new Error('Livro não encontrado');
            }

            if(livro.estaDisponivel === false) {
                throw new Error('Livro não disponível para empréstimo');
            }

            const update = {
                $set: {
                    estaDisponivel: false,
                    emprestadoPara: "fulano@email.com",
                    dataDoEmprestimo: new Date()
                }
            };

            const options = { returnOriginal: false };
            const result = await client.db('sgb').collection('livros').findOneAndUpdate(query, update, options);
            console.log(`[emprestimoLivro] result: ${JSON.stringify(result)}`);
            
            return result;
        } catch (error) {
            console.log('Ocorreu um erro: ', error);
            throw error;
        }
    }

    static async devolucaoLivro(id) {
        console.log(`[devolucaoLivro]`);

        try {
            const ObjectId = require('mongodb').ObjectId;
            const query = { _id: new ObjectId(id) };
            console.log(`[devolucaoLivro] ID: ${JSON.stringify(query)}`);

            const livro = await client.db('sgb').collection('livros').findOne(query);

            if (!livro) {
                throw new Error('Livro não encontrado');
            }

            if(livro.estaDisponivel === true) {
                throw new Error('Livro não tem empréstimo cadastrado');
            }

            const update = {
                $set: {
                    estaDisponivel: true,
                    emprestadoPara: null,
                    dataDoEmprestimo: null
                }
            };

            const options = { returnOriginal: false };
            const result = await client.db('sgb').collection('livros').findOneAndUpdate(query, update, options);

            console.log(`[devolucaoLivro] ID: ${id}`);
            console.log(`[devolucaoLivro] result: ${JSON.stringify(result)}`);
            
            return result;
        } catch (error) {
            console.log('Ocorreu um erro: ', error);
            throw error;
        }
    }

}
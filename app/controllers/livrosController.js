const joi = require('joi');
const LivrosModel = require("../models/livrosModel");

const schema = joi.object().keys({
   titulo: joi.string().required().min(3).max(50),
   autor: joi.string().required().min(3).max(50),
   ano: joi.number().required(),
   usuario: joi.string().required().min(3).max(50),
   senha: joi.string().required().min(3).max(50)
})

const admin = "@admin"
const senhaAdmin = "senhaAdmin"

module.exports = class LivrosController {

   static async getLivros(req, res, next) {
      console.log('[LivrosController] getLivros');
      try {
         const livros = await LivrosModel.getLivros();
         if (livros.length === 0) {
            res.status(404).json(`Não existe livro cadastrado.`);
            return;
         }
         livros.forEach(livro => {
            console.log(`[getLivros: retorno do banco] ${livro.titulo}`);
         });
         res.status(200).json(livros);
      } catch (error) {
         console.log(`[LivrosController getLivros Error] ${error}`);
         res.status(500).json({ error: error })
      }
   }

   static async getLivro(req, res, next) {
      console.log('[LivrosController] getLivro');
      try {
         const livro = await LivrosModel.getLivro(req.params.id);
         if (livro.length === 0) {
            res.status(404).json(`Não existe livro cadastrado.`);
            return;
         }

         console.log(`[getLivro: retorno do banco] ${livro.titulo}`);
         res.status(200).json(livro);
      } catch (error) {
         console.log(`[LivrosController getLivro Error] ${error}`);
         res.status(500).json({ error: error })
      }
   }

   static async addLivro(req, res, next) {
      console.log('[LivrosController] addLivro');
      const { error, value } = schema.validate(req.body);

      if(req.body.usuario != admin || req.body.senha != senhaAdmin) {
         res.status(403).json("Usuário não autorizado a adicionar livros");
         return;
      }

      if (error) {
         const result = {
            msg: 'Livro não incluído. Campos não foram preenchidos corretamento',
            error: error.details
         };
         res.status(500).json(result);
         return;
      }

      try {
         const addedLivro = await LivrosModel.addLivro(req.body);
         res.status(200).json(addedLivro);
      } catch (error) {
         res.status(500).json({ error: error })
      }
   }

   static async updateLivro(req, res, next) {
      console.log('[LivrosController] updateLivro');
      const { error, value } = schema.validate(req.body);

      if(req.body.usuario != admin || req.body.senha != senhaAdmin) {
         res.status(403).json("Usuário não autorizado a atualizar livros");
         return;
      }

      if (error) {
         const result = {
            msg: 'Filme não atualizado. Campos não foram preenchidos corretamento',
            error: error.details
         };
         res.status(500).json(result);
         return;
      }

      try {
         const updatedLivro = await LivrosModel.updateLivro(req.params.id, req.body);
         res.status(200).json(updatedLivro.value);
      } catch (error) {
         res.status(500).json({ error: error });
      }
   }

   static async deleteLivro(req, res, next) {
      console.log('[LivrosController] deleteLivro');

      if(req.body.usuario != admin || req.body.senha != senhaAdmin) {
         res.status(403).json("Usuário não autorizado a deletar livros");
         return;
      }

      try {
         const result = await LivrosModel.deleteLivro(req.params.id);
         res.status(200).json(result);
      } catch (error) {
         res.status(500).json({ error: error });
      }
   }


   static async emprestimoLivro(req, res, next) {
      console.log('[LivrosController] emprestimoLivro');

      try {
         const livroEmprestado = await LivrosModel.emprestimoLivro(req.params.id);
         res.status(200).json(livroEmprestado.value);
      } catch(error) {
         console.error('[LivrosController] emprestimoLivro Error:', error);
         return res.status(404).json({ error: error.message });
      }
   }

   static async devolucaoLivro(req, res, next) {
      console.log('[LivrosController] devolucaoLivro');

      try {
         const livroDevolvido = await LivrosModel.devolucaoLivro(req.params.id);
         res.status(200).json(livroDevolvido.value)
      } catch(error) {
         console.error('[LivrosController] devolucaoLivro Error:', error);
         return res.status(404).json({ error: error.message });
      }
   }
}
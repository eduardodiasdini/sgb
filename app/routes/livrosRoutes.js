const LivrosController = require('../controllers/livrosController');

module.exports = {
  getLivros: (app) => {
    app.get('/api/livros', LivrosController.getLivros);
  },
  getLivro: (app) => {
    app.get('/api/livros/:id', LivrosController.getLivro);
  },
  addLivro: (app) => {
    app.post('/api/livros', LivrosController.addLivro);
  },
  updateLivro: (app) => {
    app.put('/api/livros/:id', LivrosController.updateLivro);
  },
  deleteLivro: (app) => {
    app.delete('/api/livros/:id', LivrosController.deleteLivro);
  },
  emprestimoLivro: (app) => {
    app.put('/api/livros/emprestimo/:id', LivrosController.emprestimoLivro);
  },
  devolucaoLivro: (app) => {
    app.put('/api/livros/devolucao/:id', LivrosController.devolucaoLivro);
  }
}
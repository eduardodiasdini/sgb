console.log('[Config] Server');
let express = require('express'); 

let app = express(); 
let port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, function () {
	console.log('Servidor rodando com express na porta', port);
});

module.exports = app;